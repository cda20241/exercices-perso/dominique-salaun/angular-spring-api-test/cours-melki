import { Injectable } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AccountService } from './account.service';

@Injectable({
  providedIn :'root'
})

export class authGuard {

  constructor( 
    private router : Router,
    private accountService : AccountService){}

CanActivate(){
  this.alreadyUserLogin();
}
alreadyUserLogin(): boolean{

  if(this.accountService.isLogged()){
    return true
  }else{
    this.router.navigate(['/login'])
    return false
  }
}
};
