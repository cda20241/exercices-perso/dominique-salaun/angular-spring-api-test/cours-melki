import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MainComponent } from './main/main.component';
import { RouterOutlet } from '@angular/router';
import { LoginComponent } from '../authentification/login/login.component';



@NgModule({
  declarations: [
    
    HeaderComponent,
    FooterComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    RouterOutlet,
    
  ],

  exports: [
    HeaderComponent,
    MainComponent,
    FooterComponent
  ]

})
export class LayoutsModule { }
