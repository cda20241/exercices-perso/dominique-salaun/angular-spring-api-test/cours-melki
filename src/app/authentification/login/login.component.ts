import { Component } from '@angular/core';
import { Account,AccountImpl } from 'src/app/interfaces/account';
import { HttpClient } from '@angular/common/http';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  /** le compte de l'enseignant */

  /** Le constructeur */
  constructor(
    private http: HttpClient,
    private accountService: AccountService,
    ) {}

  /**
   * Au chargement du composant on supprime le token du precedant compte
   */
  ngOnInit(): void {
  }

  /**
   * Effectue une tentative de connexion en utilisant le service de compte.
   */
  login(email: string, password: string) {
    console.log(email)
    let account = {} as Account
    account.email = email
    account.password = password
    console.log(account)
    this.accountService.login(account).subscribe(
      (account: Account) => {
        this.accountService.accountLoggedSuccessfully(account)
      },
    );
  }
}

