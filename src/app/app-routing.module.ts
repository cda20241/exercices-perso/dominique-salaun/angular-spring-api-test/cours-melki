import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './layouts/main/main.component';
import { AuthentificationModule } from './authentification/authentification.module';
import { UserModule } from './user/user.module';
import { AdminModule } from './admin/admin.module';
import { AcceuilComponent } from './user/accueil/accueil.component';


const routes: Routes = [

{path:'authentification', loadChildren: () => AuthentificationModule },
{path:'',component: MainComponent,children:[
                                            {path:'user',loadChildren:() => UserModule},
                                            {path:'admin',loadChildren:() => AdminModule},
                                                ],
                                                
},
{path:'acceuil',component : AcceuilComponent},
 {path:'**',redirectTo:'authentification',pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
