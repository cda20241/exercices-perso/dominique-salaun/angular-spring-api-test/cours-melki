import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AccountImpl } from 'src/app/interfaces/account';

@Component({
  selector: 'app-acceuil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AcceuilComponent implements OnInit{

  users: AccountImpl[] = []

  constructor(private userService: UserService){}

  ngOnInit(): void {
      this.userService.getUser().subscribe(x => console.log(x))
  }

}
