import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { AcceuilComponent } from './accueil/accueil.component';
import { LayoutsModule } from '../layouts/layouts.module';


@NgModule({
  declarations: [
    AcceuilComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    LayoutsModule
  ],exports: [
    AcceuilComponent
  ]
})
export class UserModule { }
